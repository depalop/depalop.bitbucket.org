## &nbsp;
![./](img/logo.jpg)

# spaceMuseum
L'evoluzione dell'audioguida

## Demo
<video style="max-height: 80%; max-width: 100%;" controls>
<source src='https://spacemuseum.it/assets/video/museum.mp4'>
</video>

## L'audioguida tradizionale?
obsoleta, laboriosa, ingombrante, costosa
- alti costi iniziali e di esercizio (manutenzione/furto/usura).
- interfaccia ostica.
- gestione dei contenuti complessa.
- mancanza di multimedialità (solo contenuti audio).


## Cos'e' spaceMuseum?

spaceMuseum risolve tutte le problematiche delle tradizionali audioguide e sperimenta nuovi modi di portare contenuti in maniera semplice e immediata all'utente.  

spaceMuseum è un modo innovativo di intendere la "visita guidata".

spaceMuseum non si limita ai contenuti audio, supporta contenuti multimediali (testuali e audiovisivi).

Gli utenti possono accedere ai contenuti di spaceMuseum anche successivamente alla visita.

I costi sono contenuti perchè spaceMuseum viene utilizzato sui dispositivi dei visitatori.

## Caratteristiche base

**Semplicità**: l'utente conosce già lo strumento necessario.

spaceMuseum è **immediato**, non richiede nessun tipo di conoscenza.

**Facilità** nell'aggiornamento dei contenuti multimediali da parte del museo.

## Caratteristiche avanzate

Data la duttilità dello strumento è possibile a richiesta integrare funzionalità avanzate:

- Possibilità di avere descrizioni brevi (10 secondi) o approfondimenti per opera (a scelta dell'utente).

- Creazione di "percorsi guidati" differenti (es. percorso scolaresche, percorso cronologico, percorso rinascimentale etc.).

- Raccolta dei dati relativi agli interessi dei visitatori (quali sono le opere che suscitano più interesse?)

- Condivisione sui social network dell'esperienza museale (con conseguente richiamo di visitatori).

## &nbsp;
![./](img/logo.jpg)

