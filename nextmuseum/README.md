Ciao, sono la presentazione di nextMuseum.
Uso impress.md che e' un pacchettino che trasforma da Markdown a Impress.js

Per vedere la presentazione clonami con `git clone git@bitbucket.org:depalop/nextmuseum.git`
e apri `index.html`

Per modificare la presentazione installa le dipendenze
con `npm install` e lancia `npm run dev`, 
ad ogni modifica a `presoDemo.md` verra' riscritto index.html 
quindi bastera' aggiornare il browser !

