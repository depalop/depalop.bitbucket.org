#!/usr/bin/env node  
var impress_md = require('impress.md');
var fs = require('fs');
 
  // first parameter is the path of markdown file 
  // second parameter are options 
  // return a Promise 
  impress_md('presoDemo.md', {
    marked: { //a object, the options of marked 
      gfm: true,
      tables: true,
      breaks: false,
      pedantic: false,
      sanitize: false,
      smartLists: true,
      smartypants: false
    },
    js_files: ['socket.io.js',
    'remote.js'], // the js files will attached 
    css_files:['style.css'], // the css files will attached 
  })
    .then(function(html) {
      fs.writeFile('index.html',html);
    }, function(err) {
      console.log(err);
      process.exit(2)
    })
